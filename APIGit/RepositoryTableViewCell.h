//
//  RepositoryTableViewCell.h
//  APIGit
//
//  Created by Andre Souza on 16/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface RepositoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *repositoryName;
@property (weak, nonatomic) IBOutlet UILabel *repositoryDescription;
@property (weak, nonatomic) IBOutlet AsyncImageView *ownerPic;
@property (weak, nonatomic) IBOutlet UILabel *ownerNickname;
@property (weak, nonatomic) IBOutlet UILabel *ownerFullName;
@property (weak, nonatomic) IBOutlet UILabel *forksCount;
@property (weak, nonatomic) IBOutlet UILabel *starCount;
@property (weak, nonatomic) IBOutlet UIImageView *starIcon;
@property (weak, nonatomic) IBOutlet UIImageView *forkIcon;


@end
