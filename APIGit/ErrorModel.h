
#import "JSONModel.h"

@interface ErrorModel : JSONModel

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSNumber <Optional>*status_code;

+ (ErrorModel*)parseError;

@end
