//
//  GitRepositoryModel.h
//  APIGit
//
//  Created by Andre Souza on 14/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "RepositoryModel.h"

@interface GitRepositoryModel : JSONModel

@property (nonatomic, strong) NSNumber *totalCount;
@property (nonatomic, strong) NSArray<RepositoryModel> *repositories;

@end
