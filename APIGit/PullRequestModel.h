//
//  PullRequestModel.h
//  APIGit
//
//  Created by Andre Souza on 16/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "OwnerModel.h"

@protocol PullRequestModel

@end

@interface PullRequestModel : JSONModel

@property (nonatomic,retain) NSString* pullTitle;
@property (nonatomic,retain) NSString* pullDescription;
@property (nonatomic,retain) OwnerModel* owner;

@end
