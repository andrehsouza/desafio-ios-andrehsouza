//
//  PullRequestModel.m
//  APIGit
//
//  Created by Andre Souza on 16/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import "PullRequestModel.h"

@implementation PullRequestModel

+ (JSONKeyMapper *)keyMapper {
    NSDictionary *map = @{@"title": @"pullTitle",
                          @"body": @"pullDescription",
                          @"user": @"owner"};
    
    return [[JSONKeyMapper alloc] initWithDictionary:map];
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

@end
