//
//  BaseViewController.h
//  APIGit
//
//  Created by Andre Souza on 16/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ErrorModel.h"

@interface BaseViewController : UITableViewController

-(void)displaySpinner;
-(void)dismissSpinner;
-(void) showErrorWithModel:(ErrorModel *)ErrorModel andRetryTarget:(id)target andSelector:(SEL)selector;
-(void) removeErrorView;

@end
