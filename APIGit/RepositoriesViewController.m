//
//  MasterViewController.m
//  APIGit
//
//  Created by Andre Souza on 14/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import "RepositoriesViewController.h"
#import "GitRepositoryModel.h"
#import "RepositoryTableViewCell.h"
#import "AsyncImageView.h"
#import "PullRequestViewController.h"

@interface RepositoriesViewController () {
    NSInteger currentPage;
    NSInteger totalPages;
}

@property NSMutableArray<RepositoryModel> *repositoryList;


@end

@implementation RepositoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.repositoryList = [NSMutableArray<RepositoryModel> new];
    currentPage = 1;
    totalPages = 1;
    
    [self getRepositories];
}


- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showPullList"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        RepositoryModel *repositoryModel = [self.repositoryList objectAtIndex:indexPath.row];
        PullRequestViewController *controller = (PullRequestViewController *)[[segue destinationViewController] topViewController];
        [controller setPathUrl:repositoryModel.fullName];
        [controller setRepositoryName:repositoryModel.name];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.repositoryList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RepositoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.ownerPic];
    cell.ownerPic.image = nil;
    cell.ownerPic.showActivityIndicator = NO;
    
    RepositoryModel *repositoryModel = [self.repositoryList objectAtIndex:indexPath.row];
    [cell.repositoryName setText:repositoryModel.name];
    [cell.repositoryDescription setText:repositoryModel.repositoryDescription];
    [cell.forksCount setText:[NSString stringWithFormat:@"%@", repositoryModel.forksCount]];
    [cell.starCount setText:[NSString stringWithFormat:@"%@", repositoryModel.starCount]];
    [cell.ownerNickname setText:repositoryModel.owner.nickName];
    cell.ownerPic.imageURL = [NSURL URLWithString:repositoryModel.owner.avatarUrl];
    
    UIColor *tintColor = cell.starIcon.tintColor;
    cell.starIcon.tintColor = nil;
    cell.forkIcon.tintColor = nil;
    cell.starIcon.tintColor = tintColor;
    cell.forkIcon.tintColor = tintColor;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 135;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if((indexPath.row == (self.repositoryList.count-2)) && (currentPage < totalPages)){
        currentPage++;
        [self getRepositories];
    }
}


#pragma mark - ServiceDelegate

-(void) getRepositories {
    [self displaySpinner];
    [self removeErrorView];
    [Service requestRepositoryListWithDelegate:self andPage:[[NSNumber alloc] initWithInteger:currentPage]];
}

-(void) requestCompleted:(NSDictionary *)result {
    GitRepositoryModel *gitRepositoryModel = [[GitRepositoryModel alloc] initWithDictionary:result error:nil];
    if(gitRepositoryModel) {
        totalPages = [gitRepositoryModel.totalCount integerValue];
        
        if(gitRepositoryModel.repositories && gitRepositoryModel.repositories.count > 0) {
            [self.repositoryList addObjectsFromArray:gitRepositoryModel.repositories];
        }
    }
    [self.tableView reloadData];
    [self dismissSpinner];
    [self removeErrorView];
}

-(void)requestCompletedWithError:(ErrorModel *)errorModel {
    [self dismissSpinner];
    [self showErrorWithModel:errorModel andRetryTarget:self andSelector:@selector(getRepositories)];
}

- (IBAction)touchAbout:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Sobre"
                          message:@"GitPop: Aplicativo desenvolvido para processo de seleção da Concrete Solutions.\n\nAutor: André Luiz de Oliveira Souza\nEmail: deh.osouza@gmail.com\nTelefone: (21)98229-5965"
                          delegate: nil
                          cancelButtonTitle: @"OK"
                          otherButtonTitles:nil];
    [alert show];
}


@end
