//
//  main.m
//  APIGit
//
//  Created by Andre Souza on 14/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
