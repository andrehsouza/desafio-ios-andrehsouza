
#import "ErrorModel.h"

@implementation ErrorModel

+ (ErrorModel*)parseError
{
    ErrorModel *eModel = [[ErrorModel alloc] init];
    eModel.title = NSLocalizedString(@"apiGit_generic_dialogTitle_erroInternet", nil);
    eModel.message = NSLocalizedString(@"apiGit_generic_dialogMessage_erroInternet", nil);
    return eModel;
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

@end
