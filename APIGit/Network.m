
#import "Network.h"
#import "AFURLResponseSerialization.h"

static Network *shared = nil;

@implementation Network

+ (id)shared
{
    if (shared == nil) {
        shared = [[[self class] alloc] init];
    }
    return shared;
}

+ (void)setRequestHeaders:(NSDictionary*)headers forManager:(AFHTTPRequestOperationManager*)manager
{
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    for (NSString *key in headers)
    {
        NSString *value = [headers objectForKey:key];
        [manager.requestSerializer setValue:value forHTTPHeaderField:key];
    }
}

+ (void)GET:(NSString *)URLString
                      parameters:(NSDictionary *)parameters
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [Network setRequestHeaders:[[Network shared] headers] forManager:manager];
    
    if ([[Network shared] responseSerializer]) {
        manager.responseSerializer = [[Network shared] responseSerializer];
        [[Network shared] setResponseSerializer:nil];
    }
    
    NSLog(@"\nGET %@\nHTTPRequestHeaders %@", URLString, [manager.requestSerializer HTTPRequestHeaders]);
    
    [manager GET:URLString parameters:parameters success:success failure:failure];
}

+ (void)POST:(NSString *)URLString
  parameters:(id)parameters
     success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    if ([[Network shared] responseSerializer]) {
        manager.responseSerializer = [[Network shared] responseSerializer];
        [[Network shared] setResponseSerializer:nil];
    }
    
    [Network setRequestHeaders:[[Network shared] headers] forManager:manager];
    
    NSLog(@"\nPOST %@\nHTTPRequestHeaders %@", URLString, [manager.requestSerializer HTTPRequestHeaders]);
    
    [manager POST:URLString parameters:parameters success:success failure:failure];
}


+ (void) PUT:(NSString *)URLString
  parameters:(NSDictionary *)parameters
     success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [Network setRequestHeaders:[[Network shared] headers] forManager:manager];
    
    if ([[Network shared] responseSerializer]) {
        manager.responseSerializer = [[Network shared] responseSerializer];
        [[Network shared] setResponseSerializer:nil];
    }
    
    NSLog(@"\nPUT %@\nHTTPRequestHeaders %@", URLString, [manager.requestSerializer HTTPRequestHeaders]);
    
    [manager PUT:URLString parameters:parameters success:success failure:failure];
}

//+ (void)contentsOfURL:(NSString *)URLString onImageView:(UIImageView *)imageView{
//    if(URLString == nil) return;
//    
//    NSURL *url = [[NSURL alloc] initWithString:URLString];
//    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
//    
//    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//    
//    requestOperation.responseSerializer = [AFImageResponseSerializer serializer];
//    
//    //[[AppUtil sharedUtil] addLoadingToView:imageView];
//    
//    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        //DLog(@"Downloaded image with link: %@", responseObject);
//        
//        //[[AppUtil sharedUtil] removeLoadingForView:imageView];
//        [imageView setImage:(UIImage *)responseObject];
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
////        DLog(@"Image error: %@", error);
//    }];
//    
//    [requestOperation start];
//}


@end
