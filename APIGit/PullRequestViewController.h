//
//  DetailViewController.h
//  APIGit
//
//  Created by Andre Souza on 14/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Service.h"

@interface PullRequestViewController : BaseViewController<ServiceDelegate>

@property (nonatomic,retain) NSString* repositoryName;
@property (nonatomic,retain) NSString* pathUrl;

@end

