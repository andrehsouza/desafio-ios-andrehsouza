//
//  DetailViewController.m
//  APIGit
//
//  Created by Andre Souza on 14/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import "PullRequestViewController.h"
#import "PullRequestTableViewCell.h"
#import "PullRequestModel.h"
#import "Utils.h"

@interface PullRequestViewController () 

@property NSMutableArray<PullRequestModel> *pullList;

@end

@implementation PullRequestViewController

- (void)viewDidLoad {
    self.title = self.repositoryName;
    [super viewDidLoad];
    self.pullList = [NSMutableArray<PullRequestModel> new];
    [self getPullList];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.pullList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PullRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:cell.ownerPic];
    cell.ownerPic.image = nil;
    cell.ownerPic.showActivityIndicator = NO;
    
    PullRequestModel *pullModel = [self.pullList objectAtIndex:indexPath.row];
    [cell.pullName setText:pullModel.pullTitle];
    [cell.pullDescription setText:[Utils replaceHTMLEntitiesInString:pullModel.pullDescription]];
    [cell.ownerNickname setText:pullModel.owner.nickName];
    cell.ownerPic.imageURL = [NSURL URLWithString:pullModel.owner.avatarUrl];

    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    PullRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    PullRequestModel *pullModel = [self.pullList objectAtIndex:indexPath.row];
    
    CGFloat width = cell.contentView.frame.size.width;
    CGFloat titleSize =  [Utils calculeHeightWithLabel:cell.pullName AndText:pullModel.pullTitle AndWidth:width];
    CGFloat descSize =  [Utils calculeHeightWithLabel:cell.pullDescription AndText:[Utils replaceHTMLEntitiesInString:pullModel.pullDescription] AndWidth:width];
    return titleSize + descSize + 80;
}

#pragma mark - ServiceDelegate

-(void) getPullList {
    [self displaySpinner];
    [self removeErrorView];
    [Service getPullRequestWithPath:self.pathUrl withDelegate:self];
}

-(void) requestCompleted:(NSArray *)result {
    if(result) {
        [self.pullList addObjectsFromArray:[PullRequestModel arrayOfModelsFromDictionaries: result]];
    }
    [self.tableView reloadData];
    [self dismissSpinner];
    [self removeErrorView];
}

-(void)requestCompletedWithError:(ErrorModel *)errorModel {
    [self dismissSpinner];
    [self showErrorWithModel:errorModel andRetryTarget:self andSelector:@selector(getPullList)];
}

@end
