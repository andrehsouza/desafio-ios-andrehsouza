//
//  GitRepositoryModel.m
//  APIGit
//
//  Created by Andre Souza on 14/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import "GitRepositoryModel.h"

@implementation GitRepositoryModel

+ (JSONKeyMapper *)keyMapper {
    NSDictionary *map = @{@"total_count": @"totalCount",
                          @"items": @"repositories"};
    
    return [[JSONKeyMapper alloc] initWithDictionary:map];
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

@end
