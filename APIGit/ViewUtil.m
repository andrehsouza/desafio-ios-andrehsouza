
#import "ViewUtil.h"
#import "AppDelegate.h"

@implementation ViewUtil

+ (void)addView:(UIView *)xibView toView:(UIView *)view{
    xibView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [view addSubview:xibView];
    
    NSLayoutConstraint *width =[NSLayoutConstraint
                                constraintWithItem:xibView
                                attribute:NSLayoutAttributeWidth
                                relatedBy:0
                                toItem:view
                                attribute:NSLayoutAttributeWidth
                                multiplier:1.0
                                constant:0];
    NSLayoutConstraint *height =[NSLayoutConstraint
                                 constraintWithItem:xibView
                                 attribute:NSLayoutAttributeHeight
                                 relatedBy:0
                                 toItem:view
                                 attribute:NSLayoutAttributeHeight
                                 multiplier:1.0
                                 constant:0];
    NSLayoutConstraint *top = [NSLayoutConstraint
                               constraintWithItem:xibView
                               attribute:NSLayoutAttributeTop
                               relatedBy:NSLayoutRelationEqual
                               toItem:view
                               attribute:NSLayoutAttributeTop
                               multiplier:1.0f
                               constant:0.f];
    NSLayoutConstraint *leading = [NSLayoutConstraint
                                   constraintWithItem:xibView
                                   attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:view
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0f
                                   constant:0.f];
    [view addConstraint:width];
    [view addConstraint:height];
    [view addConstraint:top];
    [view addConstraint:leading];
}

@end
