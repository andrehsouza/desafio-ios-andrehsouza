//
//  RepositoryItem.h
//  APIGit
//
//  Created by Andre Souza on 14/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "OwnerModel.h"

@protocol RepositoryModel

@end

@interface RepositoryModel : JSONModel

@property (nonatomic,retain) NSNumber* repositoryId;
@property (nonatomic,retain) NSString* name;
@property (nonatomic,retain) NSString* fullName;
@property (nonatomic,retain) NSString* repositoryDescription;
@property (nonatomic,retain) OwnerModel* owner;
@property (nonatomic,retain) NSNumber* starCount;
@property (nonatomic,retain) NSNumber* forksCount;


@end
