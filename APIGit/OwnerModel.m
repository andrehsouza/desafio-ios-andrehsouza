//
//  OwnerModel.m
//  APIGit
//
//  Created by Andre Souza on 16/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import "OwnerModel.h"

@implementation OwnerModel

+ (JSONKeyMapper *)keyMapper {
    NSDictionary *map = @{@"id": @"ownerId",
                          @"login": @"nickName",
                          @"avatar_url": @"avatarUrl"};
    
    return [[JSONKeyMapper alloc] initWithDictionary:map];
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

@end
