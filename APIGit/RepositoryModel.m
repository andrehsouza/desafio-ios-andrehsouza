//
//  RepositoryItem.m
//  APIGit
//
//  Created by Andre Souza on 14/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import "RepositoryModel.h"

@implementation RepositoryModel

+ (JSONKeyMapper *)keyMapper {
    NSDictionary *map = @{@"id": @"repositoryId",
                          @"description": @"repositoryDescription",
                          @"full_name": @"fullName",
                          @"stargazers_count": @"starCount",
                          @"forks_count": @"forksCount"};
    
    return [[JSONKeyMapper alloc] initWithDictionary:map];
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

@end
