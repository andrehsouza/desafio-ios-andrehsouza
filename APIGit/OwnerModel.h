//
//  OwnerModel.h
//  APIGit
//
//  Created by Andre Souza on 16/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol OwnerModel

@end

@interface OwnerModel : JSONModel

@property (nonatomic,retain) NSNumber* ownerId;
@property (nonatomic,retain) NSString* nickName;
//@property (nonatomic,retain) NSString* fullName;
@property (nonatomic,retain) NSString* avatarUrl;

@end
