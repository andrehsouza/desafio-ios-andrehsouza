//
//  Utils.h
//  APIGit
//
//  Created by Andre Souza on 16/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIkit.h>

@interface Utils : NSObject

+ (CGFloat) calculeHeightWithLabel:(UILabel *)label AndText:(NSString *)text AndWidth:(CGFloat) width;
+ (NSString *) replaceHTMLEntitiesInString:(NSString *) htmlString;

@end
