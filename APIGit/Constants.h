//
//  Constants.h
//  APIGit
//
//  Created by Andre Souza on 16/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

//#ifndef APIGit_Constants_h
//#define APIGit_Constants_h

#define GIT_BASE_URL @"https://api.github.com"
#define GIT_REPOSITORY_LIST [NSString stringWithFormat:@"%@/search/repositories?q=language:Java&sort=stars&page=",GIT_BASE_URL]
#define GIT_PULL_REQUEST_LIST [NSString stringWithFormat:@"%@/repos/%@/pulls", GIT_BASE_URL, @"%@"]

//#endif
