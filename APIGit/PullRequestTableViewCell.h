//
//  ForksTableViewCell.h
//  APIGit
//
//  Created by Andre Souza on 16/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface PullRequestTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *pullName;
@property (weak, nonatomic) IBOutlet UILabel *pullDescription;
@property (weak, nonatomic) IBOutlet AsyncImageView *ownerPic;
@property (weak, nonatomic) IBOutlet UILabel *ownerNickname;

@end
