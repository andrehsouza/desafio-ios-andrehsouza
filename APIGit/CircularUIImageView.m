
#import "CircularUIImageView.h"

@implementation CircularUIImageView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = self.circularImageBounds;
    if(self.withBorder) {
        self.layer.borderWidth = 4.0f;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    self.layer.masksToBounds = YES;
    self.clipsToBounds = YES;
}



@end
