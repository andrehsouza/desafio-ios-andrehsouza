
#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface CircularUIImageView : AsyncImageView

@property (nonatomic, assign) NSUInteger circularImageBounds;
@property (nonatomic, assign) BOOL withBorder;

@end
