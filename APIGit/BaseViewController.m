//
//  BaseViewController.m
//  APIGit
//
//  Created by Andre Souza on 16/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import "BaseViewController.h"
#import "ViewUtil.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

UIView *progressIndicator;
UIView *errorView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Spinner Controls

-(void)displaySpinner {
    if(!progressIndicator){
        progressIndicator = [[[NSBundle mainBundle] loadNibNamed:@"ProgressIndicatorView" owner:self options:nil] objectAtIndex:0];
        
        [ViewUtil addView:progressIndicator toView:self.view];
    }
}

-(void)dismissSpinner{
    
    if(progressIndicator){
        [progressIndicator removeFromSuperview];
        progressIndicator = nil;
    }
}

-(void) removeErrorView {
    if(errorView){
        [errorView removeFromSuperview];
        errorView = nil;
    }
}

-(void) showErrorWithModel:(ErrorModel *)ErrorModel andRetryTarget:(id)target andSelector:(SEL)selector {
    if(!errorView){
        errorView = [[[NSBundle mainBundle] loadNibNamed:@"ErrorView" owner:self options:nil] objectAtIndex:0];
        [((UILabel*) [errorView viewWithTag:10]) setText:ErrorModel.title];
        [((UILabel*) [errorView viewWithTag:20]) setText:ErrorModel.message];
        [((UIButton*) [errorView viewWithTag:30]) addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
        [ViewUtil addView:errorView toView:self.view];
    }
}

@end
