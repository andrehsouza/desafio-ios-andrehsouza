//
//  Utils.m
//  APIGit
//
//  Created by Andre Souza on 16/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (CGFloat) calculeHeightWithLabel:(UILabel *)label AndText:(NSString *)text AndWidth:(CGFloat) width {
    
    NSString *fontName = label.font.fontName;
    CGFloat fontSize = label.font.pointSize;
    CGSize constrainedSize = CGSizeMake((width - 24), 900);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont fontWithName:fontName size:fontSize], NSFontAttributeName,
                                          nil];
    
    CGRect requiredSize = [text boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attributesDictionary context:nil];
    
    return requiredSize.size.height;
}

+ (NSString *) replaceHTMLEntitiesInString:(NSString *) htmlString {
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    return htmlString;
}

@end
