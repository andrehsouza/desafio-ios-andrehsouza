//
//  MasterViewController.h
//  APIGit
//
//  Created by Andre Souza on 14/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Service.h"

@class PullRequestViewController;

@interface RepositoriesViewController : BaseViewController<ServiceDelegate>

@property (strong, nonatomic) PullRequestViewController *forksViewController;


@end

