//
//  Service.m
//  APIGit
//
//  Created by Andre Souza on 16/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import "Service.h"
#import "Constants.h"

@implementation Service

+ (NSDictionary*)getErrorFromOperation:(AFHTTPRequestOperation*)operation andError:(NSError*)error {
    id responseObject = operation.responseObject;
    
    NSLog(@"getError from response %@", operation.responseString);
    NSLog(@"Error statusCode: %ld", (long)operation.response.statusCode);
    
    if (responseObject && [responseObject isKindOfClass:[NSDictionary class]] && [responseObject objectForKey:@"message"]) {
        NSDictionary *message = [responseObject objectForKey:@"message"];
        if ([message isKindOfClass:[NSDictionary class]] && [message objectForKey:@"message"]) {
            return message;
        }else{
            return responseObject;
        }
    } else if (responseObject && [responseObject isKindOfClass:[NSData class]]) {
        responseObject = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
    }
    
    NSNumber *statusCode = [NSNumber numberWithInteger:operation.response.statusCode];
    
    if (responseObject && [responseObject objectForKey:@"text"] && [responseObject objectForKey:@"title"])
    {
        NSDictionary *msgError = @{@"message": [responseObject objectForKey:@"text"],
                                   @"title": [responseObject objectForKey:@"title"],
                                   @"status_code": statusCode};
        return msgError;
    }
    
    if (error.code == -1004 || operation.response.statusCode == 0) {
        return @{@"title": NSLocalizedString(@"apiGit_generic_dialogTitle_erroInternet", nil),
                 @"message": NSLocalizedString(@"apiGit_generic_dialogMessage_erroInternet", nil),
                 @"status_code": statusCode};
        
    } else if (operation.response.statusCode == 500 || operation.response.statusCode == 503 || operation.response.statusCode == 504) {
        return @{@"title": NSLocalizedString(@"apiGit_generic_dialogTitle_serverError", nil),
                 @"message": NSLocalizedString(@"apiGit_generic_dialogMessage_serverError", nil),
                 @"status_code": statusCode};
        
    }
    
    return @{@"title": NSLocalizedString(@"apiGit_generic_dialogTitle_genericError", nil),
             @"message": NSLocalizedString(@"apiGit_generic_dialogMessage_genericError", nil),
             @"status_code": statusCode};
    
}


+ (void)requestRepositoryListWithDelegate:(id)delegate andPage:(NSNumber*)page {
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@", GIT_REPOSITORY_LIST, page];
    
    [Network GET:requestUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSLog(@"request: %@", responseObject);
        
        if ([delegate respondsToSelector:@selector(requestCompleted:)]){
            [delegate performSelector:@selector(requestCompleted:) withObject:responseObject];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        
        ErrorModel *errorModel = [[ErrorModel alloc] initWithDictionary:[self getErrorFromOperation:operation andError:error] error:nil];
        
        if ([delegate respondsToSelector:@selector(requestCompletedWithError:)]){
            [delegate performSelector:@selector(requestCompletedWithError:) withObject:errorModel];
        }
    }];
}

+ (void)getPullRequestWithPath:(NSString *)path withDelegate:(id)delegate {
    
    NSString *requestUrl = [NSString stringWithFormat:GIT_PULL_REQUEST_LIST, path];
    
    [Network GET:requestUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject){
        
        NSLog(@"request: %@", responseObject);
        
        if ([delegate respondsToSelector:@selector(requestCompleted:)]){
            [delegate performSelector:@selector(requestCompleted:) withObject:responseObject];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        
        ErrorModel *errorModel = [[ErrorModel alloc] initWithDictionary:[self getErrorFromOperation:operation andError:error] error:nil];
        
        if ([delegate respondsToSelector:@selector(requestCompletedWithError:)]){
            [delegate performSelector:@selector(requestCompletedWithError:) withObject:errorModel];
        }
    }];
}


@end
