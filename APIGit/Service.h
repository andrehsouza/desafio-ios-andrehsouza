//
//  Service.h
//  APIGit
//
//  Created by Andre Souza on 16/11/16.
//  Copyright © 2016 Andre Souza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Network.h"
#import "AFNetworking.h"
#import "ErrorModel.h"

@protocol ServiceDelegate

@optional
- (void)requestCompleted:(NSDictionary*)result;
- (void)requestCompletedWithError:(ErrorModel*)errorModel;
- (void)requestSuccess:(AFHTTPRequestOperation*)operation withResponse:(id)responseObject;
- (void)requestFail:(AFHTTPRequestOperation*)operation withError:(NSError*)error;

@end

@interface Service : Network

@property (weak, nonatomic) id <ServiceDelegate> delegate;

+ (void)requestRepositoryListWithDelegate:(id)delegate andPage:(NSNumber*)page;
+ (void)getPullRequestWithPath:(NSString *)path withDelegate:(id)delegate;

@end
